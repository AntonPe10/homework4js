function createNewUser() {
    const firstName = prompt("Введите имя", "");
    const lastName = prompt("Введите фамилию", "");
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            const str = this.firstName.substr(0, 1);
            const login = str + this.lastName;

            return login.toLowerCase();
        }
    };

    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());

